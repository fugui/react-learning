# react-learning

#### 项目介绍
react-learning

#### 软件架构
软件架构说明


#### 安装教程
拉项目
```
git clone https://gitee.com/fugui/react-learning.git
```

安装依赖
```
cd react-learning && npm i
```
最好全局安装webpack-cli

#### 使用说明

在项目根目录里执行
```
npm run dev
```
访问 http://localhost:7788

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
