import React, { Fragment } from "react";

export default () => {
  return (
    <Fragment>
      <h1>react 16.7 Hooks</h1>
      <ol style={{ fontSize: "32px", lineHeight: "2em" }}>
        <li>
          useState
          <br />
          const [count, setCount] = useState(0); <br />
          useStates是函数, 参数是一个值,
          <br />
          可以是普通类型也可以是引用类型, 返回一个数组,
          <br />
          第一个元素是当前值(就是函数的参数, 对应以前的this.state),
          <br />
          第二个元素是修改改制的函数(对应以前的this.setState)
          <br />
        </li>
        <li>
          useEffect <br />
          useEffect(callback: Function)
          <br />
          callback在第一次 render 和每次 update 后触发;
          <br />
          callback可以执行一些带有副作用的函数, 比如rerender UI, fetch,
          也可以结合useState一起使用更新内部状态
        </li>
        <li>
          useContext
          <br />
          const context = useContext(Context);
          <br />
          接受上下文对象（从
          <a href="https://zhuanlan.zhihu.com/p/34038469">
            React.createContext
          </a>
          返回的值）并返回当前上下文值，由给定上下文的最近上下文提供程序给出。
          <br />
          当提供程序更新时，此Hook将使用最新的上下文值触发重新呈现。
        </li>
        <li>
          useReducer
          <br />
          const [state, dispatch] = useReducer(reducer, initialState);
          <br />
          reducer是个函数 (state, action): newState;
          <br />
          action是个对象, 包含type: String, payload: any
          <br />
          initialState是个值, 初始state: any
          <br />
          通过dispatch分发action,更新state; 原理同redux
        </li>
        <li>
          其他Hooks
          <br />
          <a href="https://juejin.im/post/5bda95a3f265da393b344ba4#heading-10">
            https://juejin.im/post/5bda95a3f265da393b344ba4#heading-10
          </a>
        </li>
      </ol>
    </Fragment>
  );
};
