import React, { Fragment } from 'react';

export default () => {
  return (
    <Fragment>
      <h1>生命周期</h1>
      <img
        style={{ margin: 'auto', width: '80%'}}
        src={require('./life.jpg')}
      />
      <img
        style={{ margin: 'auto', width: '80%'}}
        src={require('./new-life.jpg')}
      />
      <ol style={{ fontSize: '32px', lineHeight: '2em' }}>
        <li>
          初始化
          <ol>
            <li>设置默认props：defaultProps: Object， propsTypes: Object</li>
            <li>执行构造函数</li>
            <li><b>16不再推荐</b> 即将插入组件： componentWillMount: void</li>
            <li>
              渲染成虚拟dom： render: ReactElement， 这个方法会在存在期多次执行
            </li>
            <li>插入文本流： componentDidMount: void</li>
          </ol>
        </li>
        <li>
          存在期
          <ol>
            <li>
              <b>16不再推荐</b> 外部将新的props数据传入：componentWillReceiveProps(nextProps:
              Object): viod<br />
              <b>16替代方案</b> static getDerivedStateFromProps(props, state): Object
            </li>
            <li>
              <b>16不再推荐</b> 决定是否更新组件：shouldComponentUpdate(nextProps: Object,
              nextState: Object): Bealoon
            </li>
            <li><b>16不再推荐</b> 即将更新组件： componentWillUpdate(): void</li>
            <li>渲染成虚拟dom： render: ReactElement <br />
            <b>16提供的顶级API</b> ReactDOM.createPortal(props, DOM_NODE)
            </li>
            <li><b>16新特性</b> getSnapshotBeforeUpdate(prevProps, prevState)<br />
              update发生的时候，在render之后，在组件dom渲染之前；返回一个值，作为componentDidUpdate的第三个参数；配合componentDidUpdate, 可以覆盖componentWillUpdate的所有用法
            </li>
            <li>完成更新组件： componentDidUpdate(): void</li>
            <li><b>16新特性</b> 异常处理： componentDidCatch(error, info): void</li>
          </ol>
        </li>
        <li>
          销毁期
          <ol>
            <li>组件即将移出文本流：componentWillUnmount(): viod</li>
          </ol>
        </li>
        <li>
          虚拟dom
          <ol>
            <li>内存里的数据操作， 比真实dom快</li>
            <li>本质是对dom节点的描述对象</li>
            <li>diff算法，比较新旧两棵dom数据的差异，并对真实dom树做最小程度的修改</li>
          </ol>
        </li>
      </ol>
    </Fragment>
  );
};
